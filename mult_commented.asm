30 
01 LDN 31 --initialization (first three lines), store original counter value at line 00
02 STO 00
03 STO 31
04 CMP 	  --make sure that counter is positive (line 31), if not, convert it to positive
05 LDN 31 --line 05, starting point of multiplication algorithm
06 STO 31
07 LDN 31
08 SUB 29 --decrement counter by one and...
09 STO 31
10 CMP 	  --...test if end of loop (if loop is finished result is on line 28)
11 JRP 29
12 JMP 27 --jump to sign and result testing
13 LDN 28 --next 5 lines performs addition, storing result on line 28
14 SUB 30
15 STO 28
16 LDN 28
17 STO 28 
18 JMP 26 --goto line 5, perform addition loop one more time
19 LDN 00 --load original B value and test sign, and flip sign accordingly (if needed)
20 CMP
21 STP
22 LDN 28
23 STO 28
24 STP 	  --halt, program is complete
26 NUM 4  --jump address
27 NUM 18 --jump address
28 NUM 0  --starting with zero, here is result of A*B
29 NUM 1  --constant used for jumps and decrement
30 NUM 64 --number A
31 NUM -3 --number B, also serves as counter
 