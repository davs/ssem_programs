# Intro


Few programs for SSEM emulator, I've used [Dave Sharp's emulator](http://www.davidsharp.com/baby/index.html) , but any _should_ work (in fact programs should work
on a replica computer as well).

Oh, if you're wondering SSEM stands for Small Scale Experimental Machine, better known as Manchester Baby, or world's first
stored program computer, take a look at [wikipedia article](http://en.wikipedia.org/wiki/Manchester_Small-Scale_Experimental_Machine).


# Description of programs

* _house.asm_  Programs that draws a house on the screen
* _mult.asm_  Since I haven't found an integer multiplication routine, I wrote one, paramaters are at lines 30 and 31, result at line 28
* _mult_commented.asm_ Multiplication routine program with comments  
 